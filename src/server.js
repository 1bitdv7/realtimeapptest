const Express  = require("express");


const app = Express()

app.use(Express.static(__dirname +'/public'))


const http = require('http').Server(app)
const ServerSocket = require('socket.io')(http)


const port = process.env.PORT || 9000

const host = "http://localhost"
http.listen(port, ()=>{
    const ports = port === 80 ? '' : ':' +port
    console.log(`${host} ${port}`)
})

app.get('/',(request,response)=> response.sendFile(__dirname+'/index.html'))

ServerSocket.on('connect',receberConexaoUser)

function receberConexaoUser(socket){
    socket.on('login',(nickname)=> registrarUsuario(socket,nickname))
    socket.on('disconnect',()=>{ console.log('Cliente desconectado'+ socket.nickname)})
    socket.on('chat',(msg)=>encaminharMsgUsuario(socket,msg))
    socket.on('status',(msg)=> encaminharMsgstatus(socket,msg))
}

function encaminharMsgstatus(socket,msg){
    socket.broadcast.emit('status',msg)
}

function encaminharMsgUsuario(socket,msg){
    ServerSocket.emit('chat',`${socket.nickname} diz: ${msg}`)
}
function registrarUsuario(socket,nickname){
    socket.nickname = nickname
    const msg = nickname + ' conectou'
    ServerSocket.emit('chat',msg)
}